const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    phoneNumber: String,
    city: String,
    email: String,
    street: String,
    phone: String,
    payment: String
  });
module.exports = orderSchema