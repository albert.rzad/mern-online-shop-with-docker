const jwt = require('jsonwebtoken')

function tokenVerification(req, res, next) {

let token = req.headers["x-access-token"];
if (!token) {
return res.status(403).send({ message: "No token provided!" });
}

jwt.verify(token, "haslo123", (err, decodeduser) => {
if (err) {
return res.status(401).send({ message: "Token niepoprawny!" });
}
req.user = decodeduser
next()
})
}
module.exports = tokenVerification